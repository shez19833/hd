# Requirements

* Api documentations
* TDD approach – writing unit tests
* Creating apis
* Consuming apis
* Code writing skills – coding standard

# Setup Instructions

    * composer install
    * cp .env.example .env
    * php artisan key:generate
    * Install yarn if you dont have it: https://yarnpkg.com/lang/en/docs/install
    * yarn install
    * yarn run dev

You will need to add your own database credentials to the `.env` file, also add the 
URLs (App & Api, which for this purpose are same but api has api/ at the end, dont 
forget to add the trailing slash `/` at the end.)

    php artisan migrate --seed (to configure database/dummy data)

Now you can serve it in your usual development environment, such as
a v-host, homestead, MAMP, etc., or simply by using `php artisan serve`.

# Running instructions

    * Once you start up and navigate to the route URL, you will see a list of units that 
    can be dispatched/returned.
    
    * There is a corresponding button to the side allowing you to change the status
    
    * At the bottom there is another section 'dispatches'  which just list all of the 
    actions you do on the route page

# Tests

    * on cli, navigate to the project root folder 
    * Run vendor/phpunit/phpunit/phpunit - hopefully you will see all green 