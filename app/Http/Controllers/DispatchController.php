<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Api\Models\Status;
use App\Repository\RepositoryInterface;

class DispatchController extends Controller
{
    /**
     * @var RepositoryInterface
     */
    private $repository;

    /**
     * DispatchController constructor.
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $dispatches = $this->repository->getAllDispatches();

        foreach ($dispatches as &$item)
        {
            $item = $this->transform($item);
        }

        return view('dispatches', [
            'requestTime' => Carbon::now(),
            'dispatches' => $dispatches,
        ]);
    }

    // could be in another file..
    private function transform($item)
    {
        $status_id = $item['status']['id'];

        return [
            'unit_name' => $item['unit']['name'],
            'transition' => $status_id === Status::DISPATCHED ? 'dispatch' : 'return',
            'isDispatching' => $status_id === Status::DISPATCHED ? true : false,
            'date_created' => Carbon::parse($item['created_at']),
        ];
    }
}
