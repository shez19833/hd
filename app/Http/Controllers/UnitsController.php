<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Repository\RepositoryInterface;

class UnitsController extends Controller
{
    private $repository;

    /**
     * UnitsController constructor.
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $units = $this->repository->getAllUnits();

        foreach ($units as &$unit)
        {
            $unit = $this->transform($unit);
        }

        return view('units', [
            'requestTime' => Carbon::now(),
            'units' => $units,
        ]);
    }

    private function transform($unit)
    {
        $dispatchedCount = $this->repository->getUnitDispatchedCount($unit['id']);
        $isDispatched = $this->repository->getUnitStatus($unit['id']) === 'dispatched' ?
            true : false;

        return [
            'unit_id' => $unit['id'],
            'dispatches' => $dispatchedCount,
            'dispatchText' => ($dispatchedCount> 0 ? ' ' : '') .
                trans_choice('units.dispatchText', $dispatchedCount),
            'status' => $isDispatched ? 'customer' : 'warehouse',
        ];
    }
}
