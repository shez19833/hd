<?php

namespace App\Repository;

use GuzzleHttp\Client;

class Api implements RepositoryInterface
{
    private $client;

    public function __construct()
    {
        $this->client = new Client([
           'base_uri' => config('app.api_url')
        ]);
    }

    public function getAllUnits()
    {
        return $this->request('GET', 'units');
    }

    public function getAllDispatches()
    {
        return $this->request('GET', 'dispatches');
    }

    public function getUnitDispatchedCount(int $id)
    {
        $url = sprintf('units/%d/dispatches/count', $id);

        return $this->request('GET', $url)['count'];
    }

    public function getUnitStatus(int $id)
    {
        $url = sprintf('units/%d/status', $id);

        return $this->request('GET', $url)['status'];
    }

    private function request($method='GET', $url, $body=[])
    {
        $response = $this->client->request($method, $url, $body);

        return json_decode($response->getBody()->getContents(), 2)['data'];
    }
}