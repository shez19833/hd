<?php

namespace App\Repository;

interface RepositoryInterface
{
    public function getAllUnits();
    public function getAllDispatches();
    public function getUnitDispatchedCount(int $unitId);
    public function getUnitStatus(int $unitId);
}