<?php

namespace App\Api\Controllers;

use App\Http\Controllers\Controller;
use App\Api\Repositories\UnitRepository;

class DispatchCountController extends Controller
{
    /**
     * @param int $id
     * @param UnitRepository $unitRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(int $id, UnitRepository $unitRepository)
    {
        $unit = $unitRepository
            ->setRelations(['dispatches'])
            ->find($id);

        return response()->json(['data' => ['count' => $unit->dispatches->count()]]);
    }
}
