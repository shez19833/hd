<?php

namespace App\Api\Controllers;

use App\Api\Resources\UnitsResource;
use App\Http\Controllers\Controller;
use App\Api\Repositories\UnitRepository;

class UnitsController extends Controller
{
    /**
     * @param UnitRepository $unitRepository
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(UnitRepository $unitRepository)
    {
        return UnitsResource::collection($unitRepository->all());
    }
}
