<?php

namespace App\Api\Controllers;

use App\Api\Models\Unit;
use App\Api\Models\Status;
use App\Http\Controllers\Controller;
use App\Api\Resources\DispatchResource;
use App\Api\Repositories\UnitRepository;
use App\Api\Repositories\DispatchRepository;

class DispatchController extends Controller
{


    /**
    * @param DispatchRepository $dispatchRepository
    * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    */
    public function index(DispatchRepository $dispatchRepository)
    {
        $items = $dispatchRepository
            ->setRelations(['unit', 'status'])
            ->all();

        return DispatchResource::collection($items);
    }

    /**
     * @param int $id
     * @param string $status
     * @param UnitRepository $unitRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(int $id, string $status, UnitRepository $unitRepository)
    {
        if ( ! in_array($status, ['dispatch', 'return']) ) {
            abort(422);
        }

        $status_id = $status === 'dispatch' ? Status::DISPATCHED : Status::RETURNED;

        $unit = $unitRepository
            ->setRelations(['status'])
            ->find($id);

        if ( $this->ifChangeAllowed($unit, $status_id) ) {
            $unit->dispatches()->attach($status_id);
        } else {
            return response()->json([], 422);
        }

        return response()->json([], 201);
    }

    /**
     * @param $unit
     * @param $status_id
     * @return bool
     */
    private function ifChangeAllowed(Unit $unit, int $status_id) : bool
    {
        if ( ! $unit->status ) {
            if ( $status_id === Status::DISPATCHED ) {
                return true;
            }
        } else if ( $unit->status->status_id !== $status_id ) {
            return true;
        }

        return false;
    }
}
