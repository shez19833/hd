<?php

namespace App\Api\Controllers;

use App\Api\Models\Status;
use App\Http\Controllers\Controller;
use App\Api\Repositories\UnitRepository;


class UnitStatusController extends Controller
{
    /**
     * @param $id
     * @param UnitRepository $unitRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($id, UnitRepository $unitRepository)
    {
        $unit = $unitRepository
            ->setRelations(['status'])
            ->find($id);

        if ($unit->status) {
            $status = $unit->status->status->name;
        } else {
            $status = Status::getName(Status::RETURNED);
        }

        return response()->json(['data' => ['status' => $status]]);
    }
}
