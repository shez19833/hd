<?php

namespace App\Api\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UnitsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $array = [
            'id' => (int) $this->id,
            'icon' => (string) $this->icon,
            'name' => (string) $this->name,
        ];

        return $array;
    }
}
