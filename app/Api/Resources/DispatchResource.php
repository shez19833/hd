<?php

namespace App\Api\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DispatchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (int) $this->id,
            'created_at' => (string) $this->created_at,
            'unit' => new UnitsResource($this->unit),
            'status' => new StatusResource($this->status),
        ];
    }
}
