<?php

namespace App\Api\Repositories;

use App\Api\Models\Dispatch;

class DispatchRepository extends BaseRepository
{
    protected $model;

    public function __construct(Dispatch $dispatch)
    {
        $this->model = $dispatch;
    }
}