<?php

namespace App\Api\Repositories;

use App\Api\Models\Unit;

class UnitRepository extends BaseRepository
{
    protected $model;

    public function __construct(Unit $unit)
    {
        $this->model = $unit;
    }
}