<?php

namespace App\Api\Repositories;

abstract class BaseRepository
{
    public $relations = [];
    protected $model;

    /**
     * @param null $relations
     * @return $this
     */
    public function setRelations($relations = null)
    {
        $this->relations = $relations;

        return $this;
    }

    /**
     * @return string
     */
    private function getCacheKey()
    {
        return $this->model . count($this->relations) > 0 ?
            implode('-', $this->relations) . '' :
            '';
    }

    /**
     * @return mixed
     */
    public function all()
    {
        $cache_key = $this->getCacheKey() . '-all';

        return \Cache::rememberForever($cache_key, function()
        {
            return $this->model
            ->with($this->relations)
            ->get();
        });
    }

    /**
     * @param $input
     * @return mixed
     */
    public function create($input)
    {
        $model = $this->model;
        $model->fill($input);
        $model->save();

        return $model;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        $cache_key = $this->getCacheKey() . '-find';

        return \Cache::rememberForever($cache_key, function() use ($id)
        {
            return $this->model->where('id', $id)->firstOrFail();
        });
    }

    /**
     * @param $id
     * @param array $input
     * @return mixed
     */
    public function update($id, array $input)
    {
        $model = $this->find($id);
        $model->fill($input);
        $model->save();

        return $model;
    }
}