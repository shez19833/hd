<?php

namespace App\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    const DISPATCHED = 1;
    const RETURNED = 2;

    public $fillable = ['name'];

    public $table = 'status';

    public $timestamps = false;

    public function units()
    {
        return $this->belongsToMany(Unit::class, 'dispatches')
            ->withPivot('created_at');
    }

    public static function getName($status_id)
    {
        switch ($status_id) {
            case 1: return 'dispatched'; break;
            case 2: return 'returned'; break;
        }

    }
}
