<?php

namespace App\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    public $fillable = ['icon', 'name'];

    public $timestamps = false;

    public function dispatches()
    {
        return $this->belongsToMany(Status::class, 'dispatches')
            ->withPivot('created_at')->latest('id');
    }

    public function status()
    {
        return $this->hasOne(Dispatch::class)->latest('id');
    }
}
