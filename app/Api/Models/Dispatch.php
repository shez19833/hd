<?php

namespace App\Api\Models;

use Illuminate\Database\Eloquent\Model;

class Dispatch extends Model
{
    public $casts = [
        'status_id' => 'int',
        'unit_id' => 'int',
    ];

    public $dates = [
        'created_at',
    ];

    public $fillable = ['status_id', 'unit_id', 'created_at'];

    public $timestamps = false;

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

}
