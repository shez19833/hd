<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('units', 'UnitsController@index');
Route::get('units/{id}/status', 'UnitStatusController@index');

Route::get('dispatches', 'DispatchController@index');
Route::post('units/{id}/{type}', 'DispatchController@store');

Route::get('units/{id}/dispatches/count', 'DispatchCountController@index');
