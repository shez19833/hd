/**
 * First we will load all of this project's JavaScript dependencies.
 */

require('./bootstrap');

(function(){
    const transitionClass = 'js-transition';
    const rowClass = 'c-row';
    const dispatchCountClass = 'js-dispatch-count';
    const dispatchTextClass = 'js-dispatch-text';

    const transitionTypes = {
        dispatch: {
            type: 'dispatch',
            text: 'Dispatch',
            next: 'return',
        },
        return: {
            type: 'return',
            text: 'Return',
            next: 'dispatch',
        },
    };

    $(`.${transitionClass}`).on('click', function(event) {
        event.preventDefault();
        attemptTransition(
            (transitionTypes[$(this).attr('data-transition')].type),
            $(this).attr('data-id'),
            $(event.target)
        );
    });

    function attemptTransition(type, $id, $this) {
        $.ajax(`/api/units/${$id}/${type}`, {
            method: 'POST',
            data: $(this).serialize()
        }).done(() => {
            performTransition(type, $this);
        }).fail((jqXHR) => {
            console.log(jqXHR);
        });
    }

    function performTransition(type, $this) {
        const newType = transitionTypes[transitionTypes[type].next];
        const newText = newType.text;

        $this
            .attr('data-transition', newType.type)
            .text(newText)
            .toggleClass('c-button--deactive', (newType.type === 'return'));

        // Update dispatches count when dispatching only
        if (type === 'dispatch') {
            const $row = $this.closest(`.${rowClass}`);
            const $dispatchCount = $row.find(`.${dispatchCountClass}`);
            const $dispatchText = $row.find(`.${dispatchTextClass}`);

            // The count is an empty string or spaces from the template, not 0
            let currentCount = parseInt($dispatchCount.text(), 10);
            if (!$dispatchCount.text().trim()) {
                currentCount = 0;
            }

            let newDispatchText = ' dispatches';
            if (currentCount === 0) {
                newDispatchText = ' dispatch';
            }

            $dispatchCount.text(currentCount + 1);
            $dispatchText.text(newDispatchText);

        }
    }

}());
