@extends('layouts.master')

@section('content')
    <div class="c-screen c-screen--primary">
        <div class="c-screen__header">
            <div class="o-wrapper">
                <div class="c-screen__header-content">
                    <div class="c-screen__title">Dispatches</div>
                    <div class="c-screen__subtitle">Accurate as of {{ $requestTime->format('G:ia') }}</div>
                </div>
            </div>
        </div>
        <div class="c-screen__body">
            <div class="c-row-container">
                @foreach ($dispatches as $dispatch)
                    <div class="c-row-container__row">
                        <div class="c-row o-wrapper">
                            <div class="o-media">
                                <div class="o-media__img">
                                    <img
                                        alt=""
                                        class="c-row__icon c-row__icon--small"
                                        src="/icons/multi-arrow-{{ $dispatch['isDispatching'] ? 'right' : 'left' }}.svg"
                                    >
                                </div>
                                <div class="o-media__body">
                                    <div class="c-row__title">{{ $dispatch['unit_name'] }}</div>
                                    <div class="c-row__subtitle">
                                        {{ $dispatch['isDispatching'] ? 'Dispatched' : 'Returned' }}
                                        at: {{ $dispatch['date_created']->format('G:ia') }}
                                        - {{ $dispatch['date_created']->format('d F Y') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="c-screen__footer">
            <div class="c-footer-buttons o-wrapper">
                <a class="c-footer-buttons__button" href="/">
                    Units
                </a>
                <a class="c-footer-buttons__button c-footer-buttons__button--active" href="/dispatches">
                    Dispatches
                </a>
            </div>
        </div>
    </div>
@endsection
