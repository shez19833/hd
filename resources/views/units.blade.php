@extends('layouts.master')

@section('content')
    <div class="c-screen c-screen--primary">
        <div class="c-screen__header">
            <div class="o-wrapper">
                <div class="c-screen__header-content">
                    <div class="c-screen__title">Units</div>
                    <div class="c-screen__subtitle">Accurate as of {{ $requestTime->format('G:ia') }}</div>
                </div>
            </div>
        </div>
        <div class="c-screen__body">
            <div class="c-row-container">
                @foreach ($units as $unit)
                    <div class="c-row-container__row">
                        <div class="c-row o-wrapper">
                            <div class="o-media">
                                <div class="o-media__img">
                                    <img alt="" class="c-row__icon" src="/icons/{{ $unit['icon'] }}.svg">
                                </div>
                                <div class="o-media__body">
                                    <div class="c-row__title">{{ $unit['name'] }}</div>
                                    <div class="c-row__subtitle">
                                        <span class="js-dispatch-count">
                                            {{ $unit['dispatches'] > 0 ? $unit['dispatches'] : '' }}
                                        </span>
                                        <span class="js-dispatch-text">
                                            {{ $unit['dispatchText'] }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="c-row__cta">
                                <div
                                    class="c-button{{
                                        $unit['status'] === 'customer' ? ' c-button--deactive' : ''
                                    }} js-transition"
                                    data-transition="{{
                                        $unit['status'] === 'customer' ? 'return' : 'dispatch'
                                    }}"
                                    data-id="{{ $unit['unit_id'] }}"
                                >
                                    {{ $unit['status'] === 'customer' ? 'Return' : 'Dispatch' }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="c-screen__footer">
            <div class="c-footer-buttons o-wrapper">
                <a class="c-footer-buttons__button c-footer-buttons__button--active" href="/">
                    Units
                </a>
                <a class="c-footer-buttons__button" href="/dispatches">
                    Dispatches
                </a>
            </div>
        </div>
    </div>
@endsection
