<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Api\Models\Unit;
use App\Api\Models\Status;
use App\Api\Models\Dispatch;
use \Illuminate\Foundation\Testing\DatabaseTransactions;

class DispatchesTest extends TestCase
{
    use DatabaseTransactions;

    public function test_i_can_get_a_list_of_dispatches()
    {
        $units = factory(Unit::class, 2)->create();

        $first = $units->first();
        $second = $units->last();

        $first_status = factory(Dispatch::class)->create([
            'unit_id' => $first->id,
            'status_id' => Status::DISPATCHED,
        ]);

        $second_status = factory(Dispatch::class)->create([
            'unit_id' => $second->id,
            'status_id' => Status::RETURNED,
        ]);

        $this->getJson('api/dispatches')
            ->assertStatus(200)
            ->assertJsonFragment([
                'id' => (int) $first_status->id,
                'created_at' => (string) $first_status->created_at,
                'unit' => [
                    'id' => (int) $first->id,
                    'icon' => (string) $first->icon,
                    'name' => (string) $first->name,
                ],
                'status' => [
                    'id' => (int) Status::DISPATCHED,
                    'name' => (string) 'dispatched',
                ]
            ])
            ->assertJsonFragment([
                'id' => (int) $second_status->id,
                'created_at' => (string) $second_status->created_at,
                'unit' => [
                    'id' => (int) $second->id,
                    'icon' => (string) $second->icon,
                    'name' => (string) $second->name,
                ],
                'status' => [
                    'id' => (int) Status::RETURNED,
                    'name' => (string) 'returned',
                ]
            ]);
    }

    public function test_i_cant_see_unit_not_dispatched()
    {
        $unit = factory(Unit::class)->create();

        $this->getJson('api/dispatches')
            ->assertStatus(200)
            ->assertJsonMissing([
                'name' => (string) $unit->name,
            ]);
    }

    public function test_i_can_dispatch_a_unit()
    {
        $unit = factory(Unit::class)->create();

        $this->postJson(sprintf('api/units/%d/%s', $unit->id, 'dispatch'))
            ->assertStatus(201);

        $this->assertEquals(1, Dispatch::whereUnitId($unit->id)->count());
    }

    public function test_i_cant_dispatch_a_unit_twice()
    {
        $unit = factory(Unit::class)->create();

        factory(Dispatch::class)->create([
            'unit_id' => $unit->id,
            'status_id' => Status::DISPATCHED,
        ]);

        $this->postJson(sprintf('api/units/%d/%s', $unit->id, 'dispatch'))
            ->assertStatus(422);

        $this->assertEquals(1, Dispatch::whereUnitId($unit->id)->count());
    }

    public function test_i_can_return_a_unit()
    {
        $unit = factory(Unit::class)->create();

        factory(Dispatch::class)->create([
            'unit_id' => $unit->id,
            'status_id' => Status::DISPATCHED,
        ]);

        $this->postJson(sprintf('api/units/%d/%s', $unit->id, 'return'))
        ->assertStatus(201);

        $this->assertEquals(1, Dispatch::whereUnitId($unit->id)->whereStatusId(Status::RETURNED)->count());
    }

    public function test_i_cant_return_a_unit_if_not_dispatched()
    {
        $unit = factory(Unit::class)->create();

        $this->postJson(sprintf('api/units/%d/%s', $unit->id, 'return'))
        ->assertStatus(422);

        $this->assertEquals(0, Dispatch::whereUnitId($unit->id)->whereStatusId(Status::RETURNED)->count());
    }
}
