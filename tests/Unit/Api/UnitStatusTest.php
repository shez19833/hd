<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Api\Models\Unit;
use App\Api\Models\Status;
use App\Api\Models\Dispatch;
use \Illuminate\Foundation\Testing\DatabaseTransactions;

class UnitStatusTest extends TestCase
{
    use DatabaseTransactions;

    public function test_i_can_get_status_of_dispatched_if_unit_is_dispatched()
    {
        $unit = factory(Unit::class)->create();

        factory(Dispatch::class)->create([
            'unit_id' => $unit->id,
            'status_id' => Status::DISPATCHED,
        ]);

        $this->getJson(sprintf('api/units/%d/status', $unit->id))
            ->assertStatus(200)
            ->assertJsonFragment([
                'status' =>  Status::getName(Status::DISPATCHED),
            ]);
    }

    public function test_status_says_warehouse_if_no_dispatches()
    {
        $unit = factory(Unit::class)->create();

        $this->getJson(sprintf('api/units/%d/status', $unit->id))
            ->assertStatus(200)
            ->assertJsonFragment([
                'status' =>  Status::getName(Status::RETURNED),
            ]);
    }

    public function test_status_says_warehouse_if_unit_returned()
    {
        $unit = factory(Unit::class)->create();

        factory(Dispatch::class)->create([
            'unit_id' => $unit->id,
            'status_id' => Status::DISPATCHED,
        ]);

        factory(Dispatch::class)->create([
            'unit_id' => $unit->id,
            'status_id' => Status::RETURNED,
        ]);

        $this->getJson(sprintf('api/units/%d/status', $unit->id))
            ->assertStatus(200)
            ->assertJsonFragment([
                'status' =>  Status::getName(Status::RETURNED),
            ]);
    }
}
