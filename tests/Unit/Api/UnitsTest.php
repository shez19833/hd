<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Api\Models\Unit;
use \Illuminate\Foundation\Testing\DatabaseTransactions;

class UnitsTest extends TestCase
{
    use DatabaseTransactions;

    public function test_i_can_get_a_list_of_units()
    {
        $unit = factory(Unit::class)->create();

        $this->getJson('api/units')
            ->assertStatus(200)
            ->assertJsonFragment([
                'id' => (int) $unit->id,
                'icon' => (string) $unit->icon,
                'name' => (string) $unit->name,
            ]);
    }
}
