<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Api\Models\Dispatch::class, function (Faker $faker) {
    return [
        'status_id' => function() {
            return factory(\App\Api\Models\Status::class)->create()->id;
        },
        'unit_id' => function() {
            return factory(\App\Api\Models\Unit::class)->create()->id;
        },
        'created_at' => $faker->dateTime,
    ];
});
