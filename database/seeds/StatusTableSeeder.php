<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['dispatched', 'returned'] as $status) {
            factory(\App\Api\Models\Status::class)->create([
               'name' => $status
            ]);
        }
    }
}
