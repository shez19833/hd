<?php

use Illuminate\Database\Seeder;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = [
            [
              'name' => 'Espen',
              'icon' => 'cube'
            ],
            [
                'name' => 'Buntan',
                'icon' => 'cloud'
            ],
            [
                'name' => 'lightbulb',
                'icon' => 'lightbulb'
            ],
        ];

        foreach ($units as $unit) {
            factory(\App\Api\Models\Unit::class)->create([
               'name' => $unit['name'],
               'icon' => $unit['icon'],
            ]);
        }
    }
}
