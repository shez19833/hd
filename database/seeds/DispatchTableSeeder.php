<?php

use Illuminate\Database\Seeder;

class DispatchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = \App\Api\Models\Unit::all(['id']);

        foreach ($units as $unit) {
            factory(\App\Api\Models\Dispatch::class, random_int(1, 5))->create([
                'unit_id' => $unit->id,
                'status_id' => \App\Api\Models\Status::DISPATCHED,
            ]);

            factory(\App\Api\Models\Dispatch::class, random_int(1, 3))->create([
                'unit_id' => $unit->id,
                'status_id' => \App\Api\Models\Status::RETURNED,
            ]);
        }
    }
}
