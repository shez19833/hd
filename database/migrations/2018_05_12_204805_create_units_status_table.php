<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispatches', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('status_id');
            $table->unsignedInteger('unit_id');
            $table->timestamp('created_at');

            $table->foreign('status_id')
                ->references('id')
                ->on('status');

            $table->foreign('unit_id')
                ->references('id')
                ->on('units');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units_status');
    }
}
